module modernc.org/lex

go 1.16

require (
	modernc.org/fileutil v1.1.2
	modernc.org/lexer v1.0.4
)
